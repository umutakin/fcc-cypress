describe("form tests", () => {
    beforeEach(() => {
        cy.visit("/forms");
    });

    it("Test subscribe form", () => {
        cy.contains(/testing forms/i);
        cy.getDataTest("subscribe-form").find("input").as("subscribe-input");
        cy.get("@subscribe-input").type("umutakin@fastmail.com");
        cy.contains(/successfully subbed: umutakin@fastmail.com!/i).should("not.exist");
        cy.getDataTest("subscribe-button").click();
        cy.contains(/successfully subbed: umutakin@fastmail.com!/i).should("exist");
        cy.wait(3000);
        cy.contains(/successfully subbed: umutakin@fastmail.com!/i).should("not.exist");

        cy.get("@subscribe-input").type("umutakin@fastmail.io");
        cy.getDataTest("subscribe-button").click();
        cy.contains(/invalid email: umutakin@fastmail.io!/i).should("exist");
        cy.wait(3000);
        cy.contains(/invalid email: umutakin@fastmail.io!/i).should("not.exist");

        cy.contains(/fail!/i).should("not.exist");
        cy.getDataTest("subscribe-button").click();
        cy.contains(/fail!/i).should("exist");
    });
});
